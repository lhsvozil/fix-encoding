import re
def ask(context: str, possibleFixes: list) -> str:
    print("=== MAKE CHOICE ===")
    print(context)
    print(*enumerate(possibleFixes), sep = '\n')
    return possibleFixes[int(input("your choice, bitch: "))]

dic = open('Czech.3-2-4.dic', encoding='cp1250')
dic = list(map(lambda w: w.strip(), dic))
stringToMend = open('forplayer.php').read()

brokenList = re.findall('\\w*[�]+[\\w�]*', stringToMend)

for bw in brokenList:
    possibleFixes = list(filter(lambda w : re.fullmatch(bw.replace('�','.'), w, re.IGNORECASE), dic))
    if len(possibleFixes) > 1:
        c = len(re.findall('[�]', bw))
        possibleFixes = list(filter(lambda w : len(re.findall('[^a-zA-Z]', w)) == c , possibleFixes))
    if len(possibleFixes) == 1:
        choice = possibleFixes[0]
    elif len(possibleFixes) > 1:
        # contxt = re.match("(.{10}"+bw+".{10})", stringToMend, re.IGNORECASE).group()
        index = stringToMend.find(bw)
        contxt = stringToMend[max(0, index-10) : min(len(stringToMend), index+10+len(bw))]
        choice = ask(contxt, possibleFixes)
        # ask(re.match("(\w+\W+"+bw+"\W+\w+)", stringToMend, re.IGNORECASE).group(), possibleFixes)
    else:
        choice = bw
        print("No suggestions :(")
    stringToMend = stringToMend.replace(bw, choice,1)

with open("out", "w") as file:
    file.write(stringToMend)